def check_metric(target_value: float,
                 real_value: float):
    try:
        assert real_value >= target_value
    except AssertionError:
        return real_value/target_value * 100
