import yaml
import torch
import logging
import numpy as np
import pandas as pd
import torchvision.transforms as transforms
from torch import nn
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from dotenv import load_dotenv
from src.data.make_dataset import MNIST
from src.models.mlp import MLP
from src.tests.metric_tests import check_metric


def main():
    # читаем конфиг файл
    with open(".config.yml", "r") as config:
        config = yaml.safe_load(config)

    # читаем env файл
    load_dotenv()

    # настройки логгера
    # инициализация Logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)  # уровень логгирования (минимальный)
    # обработчик сообщений для записи данных в файл
    logger_handler = logging.FileHandler(config['logger']['train_log_path'])
    logger_handler.setLevel(logging.INFO)  # уровень логгирования
    # Formatter для форматирования сообщений в логе
    logger_formatter = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                         datefmt='%Y-%m-%d %H:%M:%S', )
    # добавляем Formatter в обработчик сообщений
    logger_handler.setFormatter(logger_formatter)
    # добавляем обработчик сообщений в Logger
    logger.addHandler(logger_handler)

    # делаем тензор
    transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.RandomRotation(10),
        transforms.ToTensor()
    ])

    # читаем данные
    train_df = pd.read_csv(config['data']['train path'])

    # разбиваем данные на тренировочную и валидационную выборки
    X_train, X_valid, y_train, y_valid = \
        train_test_split(train_df.iloc[:, 1:], train_df['label'], test_size=1 / 6, random_state=42)

    train_dataset = MNIST(X=X_train, y=y_train, transform=transform)
    valid_dataset = MNIST(X=X_valid, y=y_valid, transform=transforms.ToTensor())

    train_loader = DataLoader(dataset=train_dataset, batch_size=config['train_model']['batch_size'], shuffle=True)
    valid_loader = DataLoader(dataset=valid_dataset, batch_size=config['train_model']['batch_size'], shuffle=False)

    dataiter = iter(train_loader)
    images, labels = next(dataiter)

    # инициализируем модель
    model = MLP()

    # инициализируем оптимизатор, функцию потерь и кол-во эмох обучения
    optimizer = torch.optim.Adam(model.parameters(), lr=config['train_model']['lr'])
    loss_fn = nn.CrossEntropyLoss()
    epochs = config['train_model']['num_epochs']

    # тренировочный цикл
    for epoch in range(epochs):
        model.train()

        train_losses = []
        valid_losses = []

        for i, (images, labels) in enumerate(train_loader):
            optimizer.zero_grad()

            outputs = model(images)
            loss = loss_fn(outputs, labels)
            loss.backward()
            optimizer.step()

            train_losses.append(loss.item())

        model.eval()
        correct = 0
        total = 0

        with torch.no_grad():
            for i, (images, labels) in enumerate(valid_loader):
                outputs = model(images)
                loss = loss_fn(outputs, labels)

                valid_losses.append(loss.item())

                _, predicted = torch.max(outputs.data, 1)
                correct += (predicted == labels).sum().item()
                total += labels.size(0)

        accuracy = correct / total

        logger.info(f'epoch : {epoch + 1,}, '
                    f'train loss : {np.mean(train_losses)}, valid loss : {np.mean(valid_losses)}, '
                    f'valid acc : {accuracy}%'
                    )

        if epoch == epochs - 1:
            result = check_metric(target_value=config['tests']['target_metric_value'],
                                  real_value=accuracy)
            logging.critical(f"Соотвествие на {result} процентов")

    return


if __name__ == '__main__':
    main()
